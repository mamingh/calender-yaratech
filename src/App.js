import React, { Component } from 'react';
import './App.css';

import Calendar from './components/Calendar';
import Note from './components/Note';
import moment from 'moment';

export default class App extends Component {
  state = {
    note: '',
    selectedDate: '',
    notes: {}
  }

  onNoteInputChange = (note) => {
    // TO-DO: method takes new note input, changes note state and adds this note for current date inside notes object
    this.setState({note: note})
    console.log(this.state.note)
  }

  onDayClick = (selectedDate) => {
    // TO-DO: method takes selected date string in DD-MM-YYYY and changes selectedDate and note accordingly
    if(this.state.note){
      this.setState({notes: this.state.note})
    }
    // this.setState({selectedDate: e.target.value})
  }

  render() {
    return (
      <div className="App">
        <Calendar onDayClick={this.onDayClick}
                  selectedDate={this.state.selectedDate} />
        <Note onNoteInputChange_func={this.onNoteInputChange}
              note__func={this.state.note}/>
      </div>
    );
  }
}
