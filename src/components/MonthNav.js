import React, { Component } from 'react'
import SelectMonth from './SelectMonth';
import moment from 'moment';

export default class MonthNav extends Component {
  state = {
    showMonthPopup: false
  }
  
  months = moment.months();
  onToggleDropdown = (e) => {
    this.setState(prevState => ({
      showMonthPopup: !prevState.showMonthPopup
    }));
    // TO-DO: method toggles showMonthPopup flag when user clicks on label
  }

  month = () => {
    // TO-DO: method returns current month name so it renders in DOM
    const dateContext = this.props.dateContext
    return(this.months[dateContext.format("M").toString()-1])
  }

  render() {
    return (
      <span className="label-month"
        onClick={(e) => {this.onToggleDropdown()}}>
        {this.month()}
        {this.state.showMonthPopup &&
          <SelectMonth data={this.months} 
                       onMonthChange={this.props.onMonthChange}/>
        }
      </span>
    )
  }
}
