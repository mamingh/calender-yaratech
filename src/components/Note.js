import React, { Component } from 'react'

export default class Note extends Component {
  constructor(props){
    super(props)
    this.state = {
      note:''
    }
  }
   onNoteInputChange = (e) => {
    this.setState({note: e.target.value})
    this.props.onNoteInputChange_func(e.target.value)
    // TO-DO: method calls parent prop handler
  }

  render() {
    return (
      <div>
        <textarea value={this.state.note}
                  onChange= {this.onNoteInputChange}
                  placeholder="Enter your note...">
        </textarea>
      </div>
    )
  }
}