import React, { Component } from 'react'

export default class YearNav extends Component {
  state = {
    showYearNav: false
  }

  year = () => {
    // TO-DO: method returns currently selected year
    console.log(this.props.dateContext.format('Y'))
    return(this.props.dateContext.format('Y'))
  }

  showYearEditor = () => {
    // TO-DO: method makes input field visible on double click of year label
    this.setState(prevState => ({
      showYearNav: !prevState.showYearNav
    }));
  }

  onKeyUpYear = (e) => {
    if ((e.which === 13 || e.which === 27) && this.isValidYear(e.target.value)) {
      this.props.onYearChange(e.target.value);
      this.setState({
        showYearNav: false
      })
    }
  }

  isValidYear = (inputYear) => {
    if(inputYear<2100 && inputYear>0){
      return(true)
    }
    // TO-DO: method validates year input
  }

  onYearChange = (e) => {
    this.props.onYearChange(e, e.target.value);
  }

  render() {
    return (
      this.state.showYearNav ?
      <input defaultValue = {''}
             className="editor-year"
             onKeyUp= {(e) => this.onKeyUpYear(e)}
             onChange = {(e) => this.onYearChange(e)}
             type="number"
             placeholder="year"/>
      :
      <span className="label-year"
            onDoubleClick={(e)=> { this.showYearEditor()}}>
            {this.year()}
      </span>
    );
  }
}
